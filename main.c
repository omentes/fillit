/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/19 11:34:28 by apakhomo          #+#    #+#             */
/*   Updated: 2017/11/19 11:34:29 by apakhomo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
/* ************************************************************************** */
/* Нам нужно создать структуру, которую мы будем прокидывать через функции    */
/* вместо нескольких переменных, например вместо карты и ее копии, сайза      */
/* поля и количества фигур. Это нужно сделать, потому что по норме не пройдем */
/* по количеству переменных в аргументах. Вся проблема в том, что там где в   */
/* коде написано (size * size) - (figure * size) по количеству переменных     */
/* в аргументах.                                                              */
/* ************************************************************************** */
int	ft_check_move(int **copy, int a, int size, int figure);

void	ft_move_line(int **copy, int a, int how_move, int size)
{
	int i;

	i = (size * size) - 1;
	while (i != how_move)
	{
		copy[a][i] = copy[a][i - how_move];
		i--;
	}
	copy[a][i] = copy[a][i - how_move];
	while (how_move > 0)
	{
		copy[a][how_move - 1] = 0;
		how_move--;
	}
}

int	ft_check_move_column(int **copy, int a, int size)
{
	int i;

	i = 0;
	i = (size * size) - 1;
	while (i > 0)
	{
		if (copy[a][i] == 1)
			return (((size * size) - 1) - i - 1);
		i--;
	}
	return (0);
}

int	ft_check_col_min(int **copy, int a, int size, int i)
{
	int x;
	int y;
	int z;

	x = ((i + 1) * size);
	y = x - size;
	z = 1;
	while (y < x)
	{
		if (copy[a][y] == 1)
			return (z);
		y++;
		z++;
	}
	return (0);
}

int	ft_check_col_max(int **copy, int a, int size, int i)
{
	int x;
	int y;
	int z;

	x = ((i + 1) * size) - 1;
	y = x - size - 1;
	z = size;
	while (y < x)
	{
		if (copy[a][x] == 1)
			return (z);
		x--;
		z--;
	}
	return (0);
}
void ft_print_copy(int *matrix, int size);

int	ft_minimal(int *f, int figure, int size)
{
	int a;
	int min;

	a = 0;
	min = figure; 
	while (a < figure * 2)
	{
		if (f[a] > 0)
		{
			if (min > f[a])
				min = f[a];
		}
		a += 2;
	}
	return (min);
}

int	ft_maximum(int *f, int figure, int size)
{
	int a;
	int max;

	a = 1;
	max = 1; 
	while (a < figure * 2)
	{
		if (f[a] > 0)
		{
			if (max < f[a])
				max = f[a];
		}
		a += 2;
	}
	return (max);
}

int	ft_check_logic(int *f, int figure, int size)
{
	int a;
	int min;
	int max;

	a = 0;
	min = ft_minimal(f, figure, size);
	max = ft_maximum(f, figure, size);
	if (max == size)
		return (size + 1 - min);
	else if (max < size)
		return (1);
	return (0);
}

int	ft_check_move(int **copy, int a, int size, int figure)
{
	int f[figure * 2];
	int i;
	int j;
	int k;
	i = 0;
	k = 0;
	while (i < figure)
	{
		f[k++] = ft_check_col_min(copy, a, size, i);
		f[k++] = ft_check_col_max(copy, a, size, i);
		i++;
	}
	k = ft_check_logic(f, figure, size);
	j = (size * size) - 1;
	while (copy[a][j] != 1)
		j--;
	if (k > 0 && k + j < size * size)
		return (k);
	return (0);
}

int	ft_move_logic(int **copy, int a, int size, int figure)
{
	int how_move;
	how_move = ft_check_move(copy, a, size, figure);
	if (how_move > 0) //fixed max map move
	{
		ft_move_line(copy, a, how_move, size);
		return (0);
	}
	//printf("how_move:_%d _+++__ a:_%d\n", how_move, a);
	if (how_move == 0 && a == 0)
		return (2);
	if (how_move == 0)
		return (1);
	return (0);
}

int	ft_check_column(int **copy, int a, int b)
{
	int i;

	i = 0;
	while (i < a)
	{
		if (copy[i][b] == 0)
			i++;
		else
			return (1);
	}
	return (0);
}

int	ft_fill(int **copy, int a, int size)
{
	int b;
	int check_count;

	b = 0;
	check_count = 0;
	while (b < size * size)
	{
		if (copy[a][b] == 1)
		{
			if (ft_check_column(copy, a, b) == 0)
				check_count++;
		}
		b++;
	}
	if (check_count == 4)
		return (0);
	else
		return (1);
}

int	**ft_copy_map(int **matrix, int size, int figure)
{
	int		**copy;
	int		a;
	int		b;
	copy = (int**)malloc(sizeof(int*) * (figure + 1));
	a = 0;
	while (a < figure)
	{
		copy[a] = (int*)malloc(sizeof(int) * (size * size));
		b = 0;
		while (b < size * size)
		{
			printf("test copy[%i][%i] %i\n",a, b, size);
			copy[a][b] = matrix[a][b];
			b++;
		}
		a++;
	}
	copy[a] = NULL;
	return (copy);
}

void	ft_copy_line(int *copy, int *matrix, int size)
{
	int a;

	a = 0;
	while (a < size * size)
	{
		copy[a] = matrix[a];
		a++;
	}
}

int	ft_calc_map(int **matrix, int **copy, int size, int figure)
{
	int a;
	int ok;

	a = 0;
	ok = 0;
	while (a < figure)
	{
		printf("a:%i\n", a);
		if (ok == 1) 							//фигура не подвигается - запускаю функцию которая
		{
			ft_copy_line(copy[a], matrix[a], size);
			if (a-- > 0)							// и даст итератору фигур -1 (и защита от сегфолта на 0 фигур)
				ok = ft_move_logic(copy, a, size, figure);	
		}
		if (ft_fill(copy, a, size) == 0) 	//если фигура стала с первого раза - то ранво 0
				a++;							//и берем следующую
		else
			ok = ft_move_logic(copy, a, size, figure); 	//двигаем ее вперед (или получаем другой ответ)
		if (ok == 1) 							//фигура не подвигается - запускаю функцию которая
		{
			ft_copy_line(copy[a], matrix[a], size);
			if (a-- > 0)							// и даст итератору фигур -1 (и защита от сегфолта на 0 фигур)
				ok = ft_move_logic(copy, a, size, figure);	
		}
		if (ok == 2)							// или же это была первая фигура и надо пересоздать карту
			return (ok);
	}
	return (0);
}



int	**ft_matrix_resize(int **matrix, int size, int figure)
{
	int		**new;
	int		a;
	int		b;
	int		c;
	int		newsize;

	newsize = size + 1;
	new = (int**)malloc(sizeof(int*) * (figure + 1));
	a = 0;
	while (a < figure)
	{
		new[a] = (int*)malloc(sizeof(int) * (size * size));
		b = 0;
		c = 0;
		while (b < size * size)
		{
			if (b != 0 && b % size == size - 1)
				new[a][b++] = 0;
			new[a][b] = matrix[a][c];
			b++;
			c++;
		}
		a++;
	}
	new[a] = NULL;
	return (new);
}

void	ft_print_result(int **copy, int figure, int size)
{
	int		a;
	int		b;
	int		c;
	char	*print_map;

	print_map = (char*)malloc(sizeof(char) * ((size * size * figure) + 1));
	a = 0;
	while (a < figure)
	{
		b = 0;
		while (b < size * size)
		{
			if (copy[a][b] == 1)
				print_map[b] = 65 + a;
			b++;
		}
		a++;
	}
	b = 0;
	while (b < size * size)
	{
		if (print_map[b] < 65)
			print_map[b] = '.';
		b++;
	}
	a = 0;
	c = 0;
	while (a < size)
	{
		b = 0;
		while (b < size)
		{
			printf("%c", print_map[c++]);
			b++;
		}
		printf("\n");
		a++;
	}
}

void	ft_fillit_logic(int **matrix, int size, int figure);

void	ft_fillit_resize(int **matrix, int size, int figure)
{
	int **new;

	new = ft_matrix_resize(matrix, size, figure);
	//free(matrix);
	ft_fillit_logic(new, size + 1, figure);
}

void	ft_fillit_logic(int **matrix, int size, int figure)
{
	int	**copy;
	int	res;

	copy = ft_copy_map(matrix, size, figure);
	res = ft_calc_map(matrix, copy, size, figure);
	// if (res != 0)
	// 	ft_fillit_resize(matrix, size, figure);
	int a = 0;
	int b = 0;
	while (a < figure)
	{
		b = 0;
		while (b < size * size)
		{
			printf("%i ", copy[a][b]);
			b++;
		}
		printf("\n");
		a++;
	}
	ft_print_result(copy, figure, size);
}


void	ft_matrix_int(void)
{
	int		mat[5][25] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


//	int		mat[5][25] = {0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// 	int		mat[6][25] = {
// 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
// 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
// 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
// 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
// 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
// };

//	int		mat[8][36] = {
	// 	1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	// };
	int		**matrix;
	int		a;
	int		b;
	int 	size = 5;
	int 	figure;

	figure = 5;
	matrix = (int**)malloc(sizeof(int*) * figure);
	a = 0;
	b = 0;
	while (a < figure)
	{
		matrix[a] = (int*)malloc(sizeof(int) * (size * size));
		a++;
	}
	a = 0;
	while (a < figure)
	{
		matrix[a] = mat[a];
		a++;
	}
	matrix[a] = NULL;
	a = 0;
	b = 0;
	while (a < figure)
	{
		b = 0;
		while (b < size * size)
		{
			printf("%i ", matrix[a][b]);
			b++;
		}
		printf("\n");
		a++;
	}
	ft_fillit_logic(matrix, size, figure);
}

int		main(int argc, char const *argv[])
{
	ft_matrix_int();
	return (0);
}

	// matrix[0] = {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	// matrix[1] = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	// matrix[2] = {1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	// matrix[3] = {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	// matrix[4] = {0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	// matrix[5] = NULL;